package com.salvador.challenge_3.data.network

import com.salvador.challenge_3.data.model.Comment
import com.salvador.challenge_3.data.model.Post
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("posts")
    fun getPostsAsync(): Deferred<List<Post>>

    @GET("comments")
    fun getCommentsAsync(@Query("postId") id: Int): Deferred<List<Comment>>

}