package com.salvador.challenge_3.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class Comment(
    @Expose
    @SerializedName("postId")
    var postId: Int,
    @Expose
    @SerializedName("id")
    @PrimaryKey
    var id: Int,
    @Expose
    @SerializedName("name")
    var name: String,
    @Expose
    @SerializedName("email")
    var email: String,
    @Expose
    @SerializedName("body")
    var body: String,
)