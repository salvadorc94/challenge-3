package com.salvador.challenge_3.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.salvador.challenge_3.data.model.Comment
import com.salvador.challenge_3.data.model.Post

@Database(entities = [Post::class, Comment::class], version = 2, exportSchema = false)
abstract class AppRoomDatabase : RoomDatabase() {

    abstract fun postAndCommentDao(): PostAndCommentsDao

    companion object {
        @Volatile
        private var instance: AppRoomDatabase? = null


        fun getDatabase(context: Context): AppRoomDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppRoomDatabase::class.java, "mDatabase")
                .fallbackToDestructiveMigration()
                .build()
    }


}
