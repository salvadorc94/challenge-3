package com.salvador.challenge_3.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class Post(
    @Expose
    @SerializedName("userId")
    var userId: Int,
    @Expose
    @SerializedName("id")
    @PrimaryKey
    var id: Int,
    @Expose
    @SerializedName("title")
    var title: String,
    @Expose
    @SerializedName("body")
    var body: String,
)