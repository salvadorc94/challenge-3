package com.salvador.challenge_3.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.salvador.challenge_3.data.model.Comment
import com.salvador.challenge_3.data.model.Post

@Dao
interface PostAndCommentsDao {

    @Query("SELECT * FROM Post")
    fun getPosts(): LiveData<List<Post>>

    @Query("SELECT * FROM Comment WHERE postId = :id")
    fun getComments(id: Int): LiveData<List<Comment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPosts(posts: List<Post>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertComments(comments: List<Comment>)

    @Query("DELETE FROM Post")
    fun deletePosts()

    @Query("DELETE FROM Comment")
    fun deleteComments()

}