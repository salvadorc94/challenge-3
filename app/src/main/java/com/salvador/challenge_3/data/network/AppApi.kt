package com.salvador.challenge_3.data.network

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object AppApi {
    private const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    val getNetwork: ApiInterface
        get() {
            val gson = GsonBuilder()
                .create()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
}