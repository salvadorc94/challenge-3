package com.salvador.challenge_3.repository

import androidx.lifecycle.LiveData
import com.salvador.challenge_3.data.local.PostAndCommentsDao
import com.salvador.challenge_3.data.model.Comment
import com.salvador.challenge_3.data.model.Post
import com.salvador.challenge_3.data.network.AppApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AppRepository(private val postAndCommentsDao: PostAndCommentsDao) {

    fun getComment(id: Int): LiveData<List<Comment>> {
        return postAndCommentsDao.getComments(id)
    }

    val allPosts: LiveData<List<Post>> = postAndCommentsDao.getPosts()

    //Refresh From Network
    suspend fun refreshPosts() {
        //Use it for things like writing and reading from db
        withContext(Dispatchers.IO) {
            val posts = AppApi.getNetwork.getPostsAsync().await()
            postAndCommentsDao.insertPosts(posts)
        }
    }

    suspend fun refreshComments(id: Int) {
        withContext(Dispatchers.IO) {
            val comments = AppApi.getNetwork.getCommentsAsync(id).await()
            postAndCommentsDao.insertComments(comments)
        }
    }


}