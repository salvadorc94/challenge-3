package com.salvador.challenge_3.util

enum class RequestStatus {
    SUCCESS,
    FAILURE,
}