package com.salvador.challenge_3.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.salvador.challenge_3.data.local.AppRoomDatabase
import com.salvador.challenge_3.repository.AppRepository
import retrofit2.HttpException

class RefreshPostWorker(appContext: Context, workerParams: WorkerParameters) :
    CoroutineWorker(appContext, workerParams) {

    companion object {
        const val WORK_NAME = "com.salvador.challenge.WORKER"
    }


    override suspend fun doWork(): Result {
        val postAndCommentsDao = AppRoomDatabase.getDatabase(applicationContext).postAndCommentDao()
        val repository = AppRepository(postAndCommentsDao)
        return try {
            repository.refreshPosts()
            Result.success()
        } catch (e: HttpException) {
            Result.retry()
        }
    }
}