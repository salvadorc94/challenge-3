package com.salvador.challenge_3.ui.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.salvador.challenge_3.data.model.Post
import com.salvador.challenge_3.databinding.PostItemBinding

class PostRecyclerAdapter(private val listener: PostItemListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface PostItemListener {
        fun onClickedPost(postId: Int)
    }

    private var items: List<Post> = emptyList() //Cached Copy of Posts

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostViewHolder(
            PostItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            listener
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PostViewHolder -> {
                holder.bind(items[position])
            }
        }
    }

    override fun getItemCount(): Int = items.size

    class PostViewHolder(
        private val itemBinding: PostItemBinding,
        private val listener: PostItemListener
    ) : RecyclerView.ViewHolder(itemBinding.root), View.OnClickListener {

        init {
            itemBinding.root.setOnClickListener(this)
        }

        fun bind(post: Post) {
            itemBinding.postTitle.text = post.title
            itemBinding.postBody.text = post.body
        }

        override fun onClick(v: View?) {
            listener.onClickedPost(adapterPosition)
        }

    }

    fun submitList(post: List<Post>) {
        items = post
        notifyDataSetChanged()
    }

}