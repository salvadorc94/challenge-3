package com.salvador.challenge_3.ui.post

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.salvador.challenge_3.data.local.AppRoomDatabase
import com.salvador.challenge_3.data.model.Post
import com.salvador.challenge_3.repository.AppRepository
import com.salvador.challenge_3.util.RequestStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostViewModel(application: Application) : AndroidViewModel(application) {

    private var repository: AppRepository

    private var _mPosts: LiveData<List<Post>>
    val mPosts: LiveData<List<Post>>
        get() = _mPosts

    val requestStatus = MutableLiveData<RequestStatus>()

    init {
        val postAndCommentsDao = AppRoomDatabase.getDatabase(application).postAndCommentDao()
        repository = AppRepository(postAndCommentsDao)
        refreshPosts()
        _mPosts = repository.allPosts
    }

    fun refreshPosts() = viewModelScope.launch(Dispatchers.IO) {
        try {
            repository.refreshPosts()
            requestStatus.postValue(RequestStatus.SUCCESS)
        } catch (e: Exception) {
            Log.i("CommentViewModel", "No Internet")
            requestStatus.postValue(RequestStatus.FAILURE)
        }
    }


}