package com.salvador.challenge_3.ui.comment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.salvador.challenge_3.data.model.Comment
import com.salvador.challenge_3.databinding.CommentItemBinding

class CommentRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<Comment> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CommentViewHolder(
            CommentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CommentViewHolder -> {
                holder.bind(items[position])
            }
        }
    }

    override fun getItemCount(): Int = items.size

    class CommentViewHolder(
        private val itemBinding: CommentItemBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(comment: Comment) {
            itemBinding.commentName.text = comment.name
            itemBinding.commentEmail.text = comment.email
            itemBinding.commentBody.text = comment.body
        }

    }

    fun submitList(comment: List<Comment>) {
        items = comment
        notifyDataSetChanged()
    }

}