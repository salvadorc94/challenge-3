package com.salvador.challenge_3.ui.comment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.salvador.challenge_3.databinding.FragmentCommentBinding
import com.salvador.challenge_3.util.TopSpacingItemDecoration
import kotlinx.android.synthetic.main.fragment_comment.*


class CommentFragment : Fragment() {

    private var _binding: FragmentCommentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var commentAdapter: CommentRecyclerAdapter

    private lateinit var viewModel: CommentViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecyclerView()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCommentBinding.inflate(inflater, container, false)

        val id = arguments?.getInt("id")
        viewModel = ViewModelProvider(this).get(CommentViewModel::class.java)

        if (id != null) {
            viewModel.getComments(id)
        }

        viewModel.mComments.observe(viewLifecycleOwner, { comments ->
            comments?.let { commentAdapter.submitList(it) }
        })

        viewModel.requestStatus.observe(viewLifecycleOwner, {
            if(it.name == "FAILURE"){
                showWarning()
            }
        })

        return binding.root
    }

    private fun showWarning(){
        Toast.makeText(context, "No Internet connection detected", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun initRecyclerView() {
        comment_recycle_view.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(TopSpacingItemDecoration(30))
            commentAdapter = CommentRecyclerAdapter()
            adapter = commentAdapter
        }
    }


}