package com.salvador.challenge_3.ui.post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.salvador.challenge_3.R
import com.salvador.challenge_3.databinding.FragmentPostBinding
import com.salvador.challenge_3.util.TopSpacingItemDecoration
import kotlinx.android.synthetic.main.fragment_post.*


class PostFragment : Fragment(), PostRecyclerAdapter.PostItemListener {

    private var _binding: FragmentPostBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var postAdapter: PostRecyclerAdapter

    //View Model
    private lateinit var viewModel: PostViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecyclerView()
        super.onViewCreated(view, savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPostBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this).get(PostViewModel::class.java)

        //Observer for PostsList
        viewModel.mPosts.observe(viewLifecycleOwner, { posts ->
            posts?.let { postAdapter.submitList(it) }
        })

        viewModel.requestStatus.observe(viewLifecycleOwner, {
            if(it.name == "FAILURE"){
                showWarning()
            }
        })

        binding.swiperefresh.setOnRefreshListener {
            myUpdateOperation()
        }

        return binding.root
    }

    private fun showWarning(){
        Toast.makeText(context, "No Internet connection detected", Toast.LENGTH_SHORT).show()
    }

    private fun myUpdateOperation() {
        viewModel.refreshPosts()
        swiperefresh.isRefreshing = false
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initRecyclerView() {
        post_recycle_view.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(TopSpacingItemDecoration(30))
            postAdapter = PostRecyclerAdapter(this@PostFragment)
            adapter = postAdapter
        }
    }

    override fun onClickedPost(postId: Int) {
        findNavController().navigate(
            R.id.action_postFragment_to_commentFragment,
            bundleOf("id" to postId)
        )
    }

}