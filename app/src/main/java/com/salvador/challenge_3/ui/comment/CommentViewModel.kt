package com.salvador.challenge_3.ui.comment

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.salvador.challenge_3.data.local.AppRoomDatabase
import com.salvador.challenge_3.data.model.Comment
import com.salvador.challenge_3.repository.AppRepository
import com.salvador.challenge_3.util.RequestStatus
import kotlinx.coroutines.launch

class CommentViewModel(application: Application) : AndroidViewModel(application) {

    private var repository: AppRepository


    private lateinit var _mComments: LiveData<List<Comment>>
    val mComments: LiveData<List<Comment>>
        get() = _mComments

    init {
        val postAndCommentsDao = AppRoomDatabase.getDatabase(application).postAndCommentDao()
        repository = AppRepository(postAndCommentsDao)
    }

    val requestStatus = MutableLiveData<RequestStatus>()

    fun getComments(id: Int) {
        viewModelScope.launch {
            _mComments = repository.getComment(id + 1)

            try {
                repository.refreshComments(id + 1)
                requestStatus.postValue(RequestStatus.SUCCESS)
            } catch (e: Exception) {
                Log.i("CommentViewModel", "No Internet")
                requestStatus.postValue(RequestStatus.FAILURE)
            }

        }
    }


}